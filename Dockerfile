FROM ubuntu:16.04

MAINTAINER fluendo

ENV DEBIAN_FRONTEND noninteractive

# Create the worker dir
RUN mkdir /worker
RUN chmod 777 /worker
WORKDIR /worker

# Install required packages
RUN apt-get update && \
    apt-get upgrade -y

# Install git
RUN apt-get install -y git

# Install Python2.7
RUN apt-get install -y \
        python2.7 \
        python2.7-dev \
        python-setuptools \
        python-pip
RUN pip install -U pip setuptools

# Install Python3.5
RUN apt-get install -y \
        python3.5 \
        python3.5-dev \
        python3-setuptools \
        python3-pip
RUN python3 -m pip install -U pip setuptools

# Install buildbot requirements
RUN pip install buildbot-worker

# Install ldap support
RUN apt-get -y install sudo wget libnss-ldap

COPY run.sh .
CMD ["./run.sh"]
